<?php

namespace App\Http\Controllers;

use App\proprietario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProprietarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proprietario = proprietario::all();
        return view('agenda.index', $proprietario);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\proprietario  $proprietario
     * @return \Illuminate\Http\Response
     */
    public function show(proprietario $proprietario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\proprietario  $proprietario
     * @return \Illuminate\Http\Response
     */
    public function edit(proprietario $proprietario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\proprietario  $proprietario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, proprietario $proprietario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\proprietario  $proprietario
     * @return \Illuminate\Http\Response
     */
    public function destroy(proprietario $proprietario)
    {
        //
    }
}
