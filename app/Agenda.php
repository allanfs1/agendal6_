<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = "agendas_teste";


    protected $fillable=['nome',
    'fone_res',
    'dt_nasc',
    'email',
    'facebook',
    'twitter',
    'instagram'
   ];


 protected $dates = [
      'dt_nasc',
      'created_at',
      'updated_at',
 ];

}
