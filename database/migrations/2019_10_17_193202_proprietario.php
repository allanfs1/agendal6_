<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Proprietario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          

        Schema::create('proprietario', function (Blueprint $table) {
            $table->bigIncrements('id_propi');
            $table->string("nome",70);
            $table->string("sexo",1);
            $table->date("dt_nasc")->nullable();
            $table->string("Carteira Ident",10)->nullable();
            $table->string("email")->nullable();
            $table->string("Naturalidade",50)->nullable();
            $table->string("Nacionalidadde",25);
            $table->string("Estado Civil",70);
            $table->string("Endereco RD",45)->nullable();
            $table->string("UF",2)->nullable();
            $table->string("orgao_Emissor",10)->nullable();
            $table->string("Tel_residencial",9);
            $table->string("profissao",45);

                 
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
