<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Imoveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
      
        Schema::create('imoveis', function (Blueprint $table) {
            $table->bigIncrements('id_imove');
            $table->string("nome",70);
            $table->string("Endereco_Resid",40);
            $table->date("dt_nasc")->nullable();
            $table->string("Naturalidade",50)->nullable();
            $table->string("Estado Civil",70);
            $table->string("cidade",45)->nullable();
            $table->string("Bairro",2)->nullable();
            $table->string("Estado",25)->nullable();
            $table->String("Filiacao",25);
            $table->String("n_dependente");
            $table->string("Tel_residencial",9);
           

                 
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
