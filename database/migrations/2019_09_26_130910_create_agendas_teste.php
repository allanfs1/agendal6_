<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTeste extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas_teste', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nome",70);
            $table->string("fone_res",15);
            $table->date("dt_nasc")->nullable();
            $table->string("email",50)->nullable();
            $table->string("facebook",75)->nullable();
            $table->string("twitter",70)->nullable();
            $table->string("instagram",70)->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas_teste');
    }
}
